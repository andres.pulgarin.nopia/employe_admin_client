/* eslint-disable */
import { User } from "@/interfaces/user";
import { AxiosResponse } from "axios";
import axios from "./axios";

export const getTasks = async (): Promise<AxiosResponse<User[]>> =>
  await axios.get("/user");

export const getTask = async (id: string): Promise<AxiosResponse<User>> =>
  await axios.get(`/user/${id}`);

export const createTask = async (user: User): Promise<AxiosResponse> =>
  await axios.post("/user", user);

export const updateTask = async (
  id: string,
  newTask: User
): Promise<AxiosResponse<User>> => await axios.put(`/user/${id}`, newTask);

export const deleteTask = async (id: string): Promise<AxiosResponse> =>
  await axios.delete(`/user/${id}`);