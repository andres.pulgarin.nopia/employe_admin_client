import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'users',
        component: () => import('@/components/userTable.vue'),
    },
    {
        path: '/users/new',
        name: 'users-new',
        component: () => import('@/components/userForm.vue'),
    },
    {
        path: '/user/:id',
        name: 'users-detail',
        component: () => import('@/components/userDetail.vue'),
    }
];

console.log(process.env.BASE_URL);

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;