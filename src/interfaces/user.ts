export interface User {
    _id: string;
    fistName: string;
    otherName: string;
    firstSurname: string;
    secondSurname: string;
    idType: string;
    idNumber: string;
    country: string;
    email: string;
    dateIn: string;
    area: string;
    state: string;
    regidterDate: string;
  }